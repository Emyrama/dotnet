# Interview Assessment

Technical assessment for DoctorLogic .NET candidates.

## Getting Started
- Visit the gitlab url for this project (https://gitlab.com/technicalinterview-doctorlogic/dotnet-test)
- Fork the project (you will need a GitLab account for this)
- Clone your fork onto your development machine
- Complete the assessment (see below)
- Push your changes to your fork
- Create a merge request in GitLab from your fork to the `technicalinterview-doctorlogic/dotnet-test` project `master` branch
- We will review your assessment and get back to you!

## Completing the Assessment

The goal of this assessment is to fix the breaking unit tests. This will mean creating or fixing the code in `InterviewAssessment`.

Assume all test inputs and expected results are valid. There are comments to give additional instruction as needed.

There may be edge cases that are not covered by these unit tests. The applicant is only expected to make the tests pass with the given inputs. They are welcome to write additional unit tests as bonus.


## Running the Assessment

In order to run this assessment, you will need to have [dotnet core](https://dotnet.microsoft.com/download) installed.

To run the tests, enter the tests project.
```
cd ./InterviewAssessment.Tests
```

Run the tests.
```
dotnet test
```
or, to run the tests as you make changes, use
```
dotnet watch test
```