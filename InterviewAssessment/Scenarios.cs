﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace InterviewAssessment
{
    public class Scenarios
    {
       // string treess="";
        /// <summary>
        /// Implement this method.
        //Done
        /// </summary>
        public string Scenario1(string str){
            char[] mArray = str.ToCharArray();
            Array.Reverse(mArray);
            string strArr=new string(mArray);
            return strArr;
        }

        /// <summary>
        /// Fix this method.
        //Done
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 0; i < exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        //Done
        /// </summary>
        public string Scenario4(Tree tree)
        {
            string StringValue = string.Empty;
            if (tree == null)
            return string.Empty;

            StringValue += tree.Text;
            foreach (var va in tree.Children)
            {
               
                if (va is Tree)
                {
                    StringValue += "-"+Scenario4((Tree)va);
                }
                else
                {
                    StringValue += "-"+va.Text;
                }
            }
        return StringValue;
        }
           
           
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
